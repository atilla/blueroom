import cv2
#from PiVideoStream import PiVideoStream

#vs = PiVideoStream().start()
cap = cv2.VideoCapture(0)
frame_width = int(cap.get(3))
frame_height = int(cap.get(4))

out = cv2.VideoWriter('appsrc ! videoconvert ! ''x264enc noise-reduction=10000 speed-preset=ultrafast tune=zerolatency ! ''rtph264pay config-interval=1 pt=96 !''tcpserversink host=127.0.0.1 port=5000 sync=false', 0, 32.0, (320, 240))

while True:
    ret, frame = cap.read()
    #frame = vs.read()
    """OpenCV algorithm that is irrelevant"""
    out.write(frame)
    key = cv2.waitKey(1)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
#vs.release()
cv2.destroyAllWindows()
