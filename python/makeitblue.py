import numpy as np
import os
import cv2
from skimage import measure
#import imutils

# Configuration
writeVideo = False
lightTreshold = 120     # Adjust depending on your lighting conditions
sobelKernel = 5

### CODE STARTS HERE ###

trailBuffer = []
trailLength = 1
tIdx = 0

kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(2,16))
kernel0 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
kernel1 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(6,6))
kernel12 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(2,4))
kernel2 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(16,16))

def cutoutbg(img):
    mask = cv2.GaussianBlur(gray, (21, 21), 3)
    return cv2.threshold(mask, lightTreshold, 255, cv2.THRESH_BINARY)[1]

def removeSmallRegions(img, size):
    labels = measure.label(img, neighbors=8, background=0)
    mask = np.zeros(img.shape, dtype="uint8")
    for label in np.unique(labels):
        if label == 0:
            continue
        labelMask = np.zeros(img.shape, dtype="uint8")
        labelMask[labels == label] = 255
        numPixels = cv2.countNonZero(labelMask)
        if numPixels > size:
            mask = cv2.add(mask, labelMask)
    return mask

def dodgeV2(image, mask):
  return cv2.divide(image, 255-mask, scale=256)

#def burnV2(image, mask):
#  return 255 – cv2.divide(255-image, 255-mask, scale=256)


# Get video stream
cap = cv2.VideoCapture(0)
frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
if writeVideo:
    out = cv2.VideoWriter('./blue_room2.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 25, (frame_width, frame_height))

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Get grayscale image
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Get background mask
    bgmask = cutoutbg(gray)
    bgmask = cv2.bitwise_not(bgmask)    # invert it
    bgmask = bgmask.astype(np.float32)  # normalize

    # Get Full face mask
    bgmask = removeSmallRegions(bgmask,20000)
    bgmask = cv2.bitwise_not(bgmask)    # invert it
    bgmask = removeSmallRegions(bgmask,30000)
    bgmask = cv2.bitwise_not(bgmask)    # invert it

    # Full color mask
    cbgmask = cv2.cvtColor(bgmask, cv2.COLOR_GRAY2BGR)  # convert to color
    #cbgmask = cbgmask.astype(np.float32) # normalize

    # edge detection mask
    ggmask = cv2.GaussianBlur(gray, (7, 7), 0)
    ggmask = cv2.Canny(ggmask,20,50)
    ggmask = cv2.morphologyEx(ggmask, cv2.MORPH_DILATE, kernel1)
    #ggmask = cv2.morphologyEx(ggmask, cv2.MORPH_ERODE, kernel1)
    #ggmask = cv2.cvtColor(ggmask, cv2.COLOR_GRAY2BGR)

    # sobel pixel shift mask
    
    #fgmask = cv2.edgePreservingFilter(frame, flags=1, sigma_s=60, sigma_r=0.4)
    #dst_gray, fgmask = cv2.pencilSketch(fgmask, sigma_s=60, sigma_r=0.07, shade_factor=0.05)
    #fgmask = cv2.GaussianBlur(fgmask, (7,7), 9)

    #fgmask = dodgeV2(fgmask,frame)
    fgmask = cv2.GaussianBlur(frame, (7,7), 9)
    fgmask = cv2.edgePreservingFilter(fgmask, flags=1, sigma_s=5, sigma_r=0.1)
    fgmask = cv2.Sobel(fgmask,cv2.CV_8UC1,1,0,ksize=3)
    #fgmask = cv2.Sobel(fgmask,cv2.CV_8UC1,1,0,ksize=sobelKernel)
    #fgmask = 255 - fgmask
    fgmask = cv2.GaussianBlur(fgmask, (0,0), 1)
    fgmask = cv2.Sobel(fgmask,cv2.CV_8UC1,1,1,ksize=7)
    fgmask = cv2.bitwise_and(fgmask, cbgmask)
    fgmask = 255 - fgmask
    fgmask = cv2.detailEnhance(fgmask, sigma_s=10, sigma_r=0.15)
    fgmask = cv2.cvtColor(fgmask, cv2.COLOR_BGR2GRAY)  # convert to gray
    fgmask = cv2.applyColorMap(fgmask, cv2.COLORMAP_OCEAN)
    #fgmask[:,:,0] -= 1
    #fgmask[:,:,2] += 5
    #fgmask = cv2.cvtColor(fgmask, cv2.COLOR_GRAY2BGR)  # convert to gray
    #fgmask[:,:,2] = 0
    
    #fgmask = 255 - fgmask

    #fgmask = cv2.cvtColor(fgmask, cv2.COLOR_GRAY2BGR)  # convert to color
    #fgmask = fgmask.astype(np.int32)      #normalize after Sobel
    #fgmask = cv2.dilate(fgmask, (3,3))
    #fgmask = cv2.GaussianBlur(fgmask, (0,0), 3)
    #fgmask = cv2.cvtColor(fgmask, cv2.COLOR_GRAY2BGR)  # convert to color
    #fgmask = cv2.Canny(fgmask,5,10)
    #fgmask = cv2.erode(fgmask, (3,3))
    #im2, contours, hierarchy = cv2.findContours(fgmask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    #cv2.drawContours(fgmask, contours, -1, (0,255,0), 3)

    #fgmask = cv2.blur(fgmask, (3,3))
    #fgmask = cv2.edgePreservingFilter(fgmask, flags=1, sigma_s=5, sigma_r=0.1)
    #dst_gray, fgmask = cv2.pencilSketch(fgmask, sigma_s=60, sigma_r=0.07, shade_factor=0.05)
    #fgmask = cv2.detailEnhance(fgmask, sigma_s=10, sigma_r=0.15)

    
    #fgmask[:,:,1] = 0
    #fgmask[:,:,2] = 0
    #fgmask = 255 - fgmask
    #fgmask = cv2.Sobel(fgmask,cv2.CV_64F,1,0,ksize=sobelKernel)

    #fgmask = cv2.bitwise_or(fgmask, fgmask)
#    fgmask[:,:,1] = 0
#    fgmask[:,:,2] = 0


    #fgmask = cv2.detailEnhance(fgmask, sigma_s=10, sigma_r=0.15)
#    fgmask = fgmask.astype(np.float32)      #normalize after Sobel
#    fgmask = cv2.GaussianBlur(fgmask, (7, 7), 0)
#    fgmask = cv2.morphologyEx(fgmask,cv2.MORPH_DILATE, kernel12)
#    fgmask = cv2.threshold(fgmask, 20, 255, cv2.THRESH_BINARY)[1]
#    fgmask = fgmask.astype(np.float32)
#    fgmask = cv2.bitwise_and(cbgmask, fgmask)
    #fgmask = cv2.stylization(frame, sigma_s=60, sigma_r=0.07)

    # Mask Mixer
    dgmask = gray.copy()
    #dgmask = cv2.bitwise_not(dgmask)
    dgmask = cv2.cvtColor(dgmask, cv2.COLOR_GRAY2BGR)  # convert to color
    #dgmask = cv2.cvtColor(dgmask, cv2.COLOR_BGR2HSV)  # convert to color
    #dgmask[:,:,1] = 255
    #dgmask[:,:,2] = 255
    #dgmask[dgmask[:,:,0] == 0] = 255

    # Trail
    # if len(trailBuffer) > trailLength:
    #     trailBuffer.remove(trailBuffer[0])
    # trailBuffer.append(dgmask)

    # #trailPic = trailBuffer[0]   # get last pic
    # i = 0
    # step = 255 / trailLength
    # trailPic = None
    # for tp in trailBuffer:
    #     tp = tp.astype(np.float32)
    #     tp[:,:,0] += tp[:,:,2] - (i * step)
    #     tp[:,:,1] += tp[:,:,2] - (i * step)
    #     #print(type(trailPic))
    #     if str(type(trailPic)) == "<class 'NoneType'>":
    #         trailPic = tp.copy()
    #     else:
    #         trailPic = cv2.bitwise_and(trailPic, tp)
    #     i += 1

    # TODO: Fix proper blur
    #dgmask = cv2.medianBlur(dgmask,5)
    #cv2.GaussianBlur(dgmask, (1,1), 10, dgmask, 10)
    #mykernel = np.ones((1,1),np.float32)/25
    #dgmask = cv2.filter2D(dgmask,-1,mykernel)
    #dgmask = cv2.blur(dgmask,(1,1))
    #dgmask = cv2.blur(dgmask,(1,1))

    #mykernel = np.ones((3,3),np.float32)/225
    #dgmask = cv2.erode(dgmask, mykernel)
    #dgmask = cv2.filter2D(dgmask,-1,mykernel)

    if writeVideo:
        out.write(trailPic)

    cv2.imshow('FG Mask',fgmask)
    cv2.imshow('GG Mask',bgmask)
    cv2.imshow('Final Pic',dgmask)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
if writeVideo:
    out.release()
cv2.destroyAllWindows()
