import numpy as np
import os
import cv2
import argparse
from skimage import measure

# Configuration
writeVideo = False
writeVideoPath = "./blue_room_capture.avi"
animateColors = False
removeBackground = False
lightTreshold = 120     # Adjust depending on your lighting conditions
sobelKernel = 5

### CODE STARTS HERE ###

# Commandline arguments parsing
parser = argparse.ArgumentParser(description='...')
parser.add_argument('--write-video', default=None, type=str, help="path to video output (if present generated visual will be saved in file with this name)")
parser.add_argument('--animate-colors', default=False, type=bool, help="Animate RGB color hues")
parser.add_argument('--remove-background', default=False, type=bool, help="Remove background noise focusing the prominent objects on scene")
parser.add_argument('--room-light-level', default=120, type=int, help="How well lighted your surroundings are. It controls how well is background removed")
parser.add_argument('--sobel-size', default=5, type=int, help="Size of Sobel kernel controling the resolution or graininess of visuals")

args = parser.parse_args()

if (args.write_video != None):
    writeVideo = True
    writeVideoPath = args.write_video
if args.animate_colors:
    animateColors = True
if args.remove_background:
    removeBackground = True
    if args.room_light_level != None:
        lightTreshold = int(args.room_light_level)
if args.sobel_size != None:
    sobelKernel = int(args.sobel_size)

trailBuffer = []
trailLength = 1
tIdx = 0

kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(2,16))
kernel0 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
kernel1 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(6,6))
kernel12 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(2,4))
kernel2 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(16,16))

def cutoutbg(img):
    mask = cv2.GaussianBlur(gray, (21, 21), 3)
    return cv2.threshold(mask, lightTreshold, 255, cv2.THRESH_BINARY)[1]

def removeSmallRegions(img, size):
    labels = measure.label(img, neighbors=8, background=0)
    mask = np.zeros(img.shape, dtype="uint8")
    for label in np.unique(labels):
        if label == 0:
            continue
        labelMask = np.zeros(img.shape, dtype="uint8")
        labelMask[labels == label] = 255
        numPixels = cv2.countNonZero(labelMask)
        if numPixels > size:
            mask = cv2.add(mask, labelMask)
    return mask

def dodgeV2(image, mask):
  return cv2.divide(image, 255-mask, scale=256)

# color animation
bitPlane = 0
alpha = 0.0

# Get video stream
cap = cv2.VideoCapture(0)
frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
if writeVideo:
    out = cv2.VideoWriter(writeVideoPath,cv2.VideoWriter_fourcc('M','J','P','G'), 25, (frame_width, frame_height))

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Get grayscale image
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Get background mask
    bgmask = cutoutbg(gray)
    bgmask = cv2.bitwise_not(bgmask)    # invert it
    bgmask = bgmask.astype(np.float32)  # normalize

    # Get Full face mask
    bgmask = removeSmallRegions(bgmask,20000)
    bgmask = cv2.bitwise_not(bgmask)    # invert it
    bgmask = removeSmallRegions(bgmask,30000)
    bgmask = cv2.bitwise_not(bgmask)    # invert it

    # Full color mask
    cbgmask = cv2.cvtColor(bgmask, cv2.COLOR_GRAY2BGR)  # convert to color
    #cbgmask = cbgmask.astype(np.float32) # normalize

    # Contours
    ggmask = cv2.GaussianBlur(gray, (7, 7), 0)
    ggmask = cv2.Canny(ggmask,20,50)
    ggmask = cv2.morphologyEx(ggmask, cv2.MORPH_DILATE, kernel1)

    # sobel pixel shift mask
    fgmask = cv2.GaussianBlur(frame, (7,7), 9)
    #fgmask = cv2.bitwise_and(fgmask, cbgmask)
    fgmask = cv2.edgePreservingFilter(fgmask, flags=1, sigma_s=5, sigma_r=0.1)
    fgmask = cv2.Sobel(fgmask,cv2.CV_8UC1,1,0,ksize=3)
    fgmask = cv2.Sobel(fgmask,cv2.CV_8UC1,0,1,ksize=sobelKernel)
    #fgmask = cv2.erode(fgmask, (7,7))
    #fgmask = cv2.edgePreservingFilter(fgmask, flags=1, sigma_s=60, sigma_r=0.4)
    fgmask = 255 - fgmask
    fgmask[:,:,0] -= 1
    fgmask[fgmask[:,:,2] < 10] = [255, 0, 0]
    fgmask[fgmask[:,:,2] == 0] = [255, 255, 255]

    #fgmask = cv2.Sobel(fgmask,cv2.CV_8UC1,1,1,ksize=7)
    #ggmask = cv2.GaussianBlur(fgmask, (0,0), 10)
    fgmask = cv2.detailEnhance(fgmask, sigma_s=10, sigma_r=0.15)
    fgmask = cv2.GaussianBlur(fgmask, (0,0), 1)
    fgmask = cv2.applyColorMap(fgmask, cv2.COLORMAP_OCEAN)
    #fgmask = cv2.bitwise_not(fgmask)

    # Blend between RGB
    if animateColors:
        if bitPlane == 0:
            # Green filter
            ggmask = fgmask.copy()
            ggmask[:,:,1] = 255
            srcMask = fgmask
            dstMask = ggmask
        if bitPlane == 1:
            # Green filter
            ggmask = fgmask.copy()
            ggmask[:,:,1] = 255
            # Red filter
            rgmask = fgmask.copy()
            rgmask[:,:,2] = 255
            srcMask = ggmask
            dstMask = rgmask
        if bitPlane == 1:
            # Red filter
            rgmask = fgmask.copy()
            rgmask[:,:,2] = 255
            srcMask = rgmask
            dstMask = fgmask

        alpha += 0.05
        if alpha < 1:
            beta = (1.0 - alpha)
        else:
            alpha = 0.0
            bitPlane += 1
            if bitPlane > 2:
                bitPlane = 0

        if alpha == 0:
            final = dstMask
        else:
            final = cv2.addWeighted(srcMask, alpha, dstMask, beta, 0.0)
    else:
        final = fgmask

    if writeVideo:
        out.write(final)

    cv2.imshow('Final',final)
    #cv2.imshow('FG Mask',fgmask)
    #cv2.imshow('GG Mask',ggmask)
    #cv2.imshow('RG Mask',rgmask)
    char = cv2.waitKey(1) & 0xFF
    
    if char == ord('q'):
        break
    else:
        if char == ord('w'):
            animateColors = True
        else:
            if char == ord('x'):
                animateColors = False

# When everything done, release the capture
cap.release()
if writeVideo:
    out.release()
cv2.destroyAllWindows()
