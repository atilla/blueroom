#include "opencv2/core/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/photo.hpp"
#include <iostream>
#include <ctype.h>
#include <X11/Xlib.h>
using namespace cv;
using namespace std;
Mat image;

// Configuration
bool writeVideo = false;
bool animateColors = false;
bool removeBackground = false;
int lightTreshold = 120, sobelKernel = 5, colorStep = 1, lastStep = 1;     // Adjust depending on your lighting conditions
string writeVideoPath = "./blue_room_capture.avi";
double alpha = 0; double beta;

bool backprojMode = false;
bool selectObject = false;
bool alphaDirection = true;
bool fullScreen = false;
int trackObject = 0;
bool showHist = true;
Point origin;
Rect selection;
int vmin = 10, vmax = 256, smin = 30;
int sc_width = 640, sc_height = 480;
int cmaps[] = {5, 6, 10};

string hot_keys =
    "\n\nHot keys: \n"
    "\tESC - quit the program\n"
    "\tp - pause video\n";

static void help()
{
    cout << "\nThis is a demo that shows mean-shift based tracking\n"
            "You select a color objects such as your face and it tracks it.\n"
            "This reads from video camera (0 by default, or the camera number the user enters\n"
            "Usage: \n"
            "   ./blueroom-visuals --animate-colors --remove-background --room-light-level --sobel-size [camera number]\n";
    cout << hot_keys;
}
static Mat cutoutbg(Mat img) {
    Mat mask, dst;
    GaussianBlur(img, mask, Size(21, 21), 3);
    threshold( mask, dst, 120, 255, 0 );
    return dst;
}

const char* keys =
{
    "{help h | | show help message}{@camera_number| 0 | camera number}"
};
int main( int argc, const char** argv )
{
    VideoCapture cap;
    Rect trackWindow;
    int hsize = 16;
    float hranges[] = {0,180};
    const float* phranges = hranges;
    CommandLineParser parser(argc, argv, keys);
    if (parser.has("help"))
    {
        help();
        return 0;
    }
    int camNum = parser.get<int>(0);
    cap.open(camNum);
    if( !cap.isOpened() )
    {
        help();
        cout << "***Could not initialize capturing...***\n";
        cout << "Current parameter's value: \n";
        parser.printMessage();
        return -1;
    }
    int im_width = cap.get(CV_CAP_PROP_FRAME_WIDTH);
    int im_height = cap.get(CV_CAP_PROP_FRAME_HEIGHT);
    printf("Width=%d, Height=%d\n", im_width, im_height);

    cout << hot_keys;
    //setMouseCallback( "Blueroom Visuals", onMouse, 0 );
    Mat frame, hsv, hue, mask, ggmask, fgmask, hist, histimg = Mat::zeros(im_height, im_width, CV_8UC3), backproj;
    Mat kernel1 = cv::getStructuringElement(MORPH_ELLIPSE,Size(6,6));
    Mat colorFilter1 = Mat::zeros(im_width, im_height, CV_8UC3), colorFilter2 = Mat::zeros(im_width, im_height, CV_8UC3);

    Mat lookUpTableZero = Mat::zeros(im_height, im_width, CV_8UC1);
    Mat lookUpTableMax(im_height, im_width, CV_8UC1, 255);
    Mat greenColMap(1, 256, CV_8UC3, Scalar(0,255,0));
    Mat lookupTableRes;

    cvNamedWindow("Blueroom Visuals", CV_WINDOW_NORMAL);
    //cvSetWindowProperty("Blueroom Visuals", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
    //cvResizeWindow("Blueroom Visuals", sc_width, sc_height);

    // Define colormap
/*    Mat lutRND(256, 1, CV_8UC3);
    for (int i=0; i<255; i++) {
        lutRND.at(i, 1) = i;
    }*/

    bool paused = false;
    for(;;)
    {
        if( !paused )
        {
            cap >> frame;
            if( frame.empty() )
                break;
        }
        //frame.copyTo(image);
        if( !paused )
        {
            cv::cvtColor(frame, mask, CV_BGR2GRAY);
            image = cutoutbg(mask);
            cv::bitwise_not(image, image);
            image.convertTo(image, CV_32F);
            // Contours
            GaussianBlur(mask, ggmask, Size(7, 7), 0);
            Canny(ggmask, ggmask, 20, 50);
            morphologyEx(ggmask, ggmask, MORPH_DILATE, kernel1);

            GaussianBlur(frame, fgmask, Size(7, 7), 9);
            edgePreservingFilter(fgmask, fgmask, 1, 5.0, 0.1);
            Sobel(fgmask, fgmask, CV_8UC1, 1, 0, 3);
            Sobel(fgmask, fgmask, CV_8UC1, 0, 1, sobelKernel);
            Mat element = getStructuringElement(MORPH_ELLIPSE, Size(7,7), Point(3, 3));
            //erode(fgmask, fgmask, element);

            fgmask = Scalar(255,255,255) - fgmask;
            inRange(fgmask, Scalar(0, 0, 0), Scalar(255, 255, 10), hue);
            fgmask.setTo(Scalar(255,0,0), hue);
            inRange(fgmask, Scalar(0, 0, 0), Scalar(255, 255, 0), hue);
            fgmask.setTo(Scalar(255,255,255), hue);

            detailEnhance(fgmask, fgmask, 10, 0.15);
            GaussianBlur(fgmask, fgmask, Size(0, 0), 1);

            if ( removeBackground )
            {
                fgmask = ggmask;
            }

            // Fade to another color
            if ( animateColors )
            {
                switch (colorStep) {
                    case 1:     // COLORMAP_OCEAN
                        applyColorMap(fgmask, hue, 5);
                        break;
                    case 2:     // COLORMAP_SUMMER
                        applyColorMap(fgmask, hue, 6);
                        break;
                    case 3:     // COLORMAP_AUTUMN
                        applyColorMap(fgmask, hue, 10);
                        break;
                }
                applyColorMap(fgmask, fgmask, cmaps[lastStep]);
                alpha = alpha + 0.05;
                if (alpha <= 1) {
                    animateColors = false;
                    lastStep = colorStep;
                }

                /*cv::cvtColor(fgmask, hist, CV_BGR2GRAY);    // Grey
                Mat channelsGreen[] = {hist, lookUpTableMax, hist};
                Mat channelsRed[] = {lookUpTableMax, hist, hist};
                Mat channelsBlue[] = {hist, hist, lookUpTableMax};

                if (alphaDirection) {
                    alpha = alpha + 0.1;
                    if (alpha >= 1) {
                        alphaDirection = true;
                        alpha = 1;
                    }
                } else {
                    alpha = alpha -0.1;
                    if (alpha <= 0) {
                        alphaDirection = false;
                        alpha = 0;
                        colorStep++;
                        if (colorStep >= 4) {
                            colorStep = 1;
                        }
                    }
                }
                switch (colorStep) {
                    case 1:
                        merge(channelsGreen, 3, hue);
                        break;
                    case 2:
                        merge(channelsRed, 3, hue);
                        break;
                    case 3:
                        merge(channelsBlue, 3, hue);
                        break;
                }*/

                beta = ( 1.0 - alpha );
                addWeighted( fgmask, alpha, hue, beta, 0.0, fgmask);
            }
            else
            {
                switch (colorStep) {
                    case 1:
                        applyColorMap(fgmask, fgmask, 5);
                        //applyColorMap(fgmask, fgmask, greenColMap); test on pi
                        break;
                    case 2:
                        applyColorMap(fgmask, fgmask, 6);
                        break;
                    case 3:
                        applyColorMap(fgmask, fgmask, 10);
                        break;
                }
            }
        }

        imshow( "Blueroom Visuals", fgmask );
        char c = (char)waitKey(10);
        if( c == 27 )
            break;
        switch(c)
        {
            case '1':
                if (lastStep == 1)
                    break;
                colorStep = 1;
                alpha = 0;
                animateColors = true;
                break;
            case '2':
                if (lastStep == 2)
                    break;
                colorStep = 2;
                alpha = 0;
                animateColors = true;
                break;
            case '3':
                if (lastStep == 3)
                    break;
                colorStep = 3;
                alpha = 0;
                animateColors = true;
                break;
            case 'f':
		if (fullScreen)
		{
			cvSetWindowProperty("Blueroom Visuals", CV_WND_PROP_AUTOSIZE, CV_WINDOW_AUTOSIZE);
			fullScreen = true;
		} else {
			cvSetWindowProperty("Blueroom Visuals", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
			fullScreen = false;
		}
		break;
            case 's':
                lightTreshold += 1;
                break;
            case 'x':
                lightTreshold -= 1;
                break;
            case 'p':
                paused = !paused;
                break;
            default:
                ;
        }
    }
    return 0;
}
