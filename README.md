# Blueroom

## Visuals
We take input from cam using OpenCV and then translate it into blue painting
like pictures.

## Sounds
Visual input obtained above is transformed into sound and then processed to 
steer our music improvisation

## How to generate sound
First launch makeitblue.py to obtain visual input from your camera and save a screenshot of your choice. Then run following code:

```
python spectroGen.py -d 20 -n 50 -x 20000 -s 44100 media/example.jpg media/example.wav
```